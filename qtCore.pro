QT += core gui widgets xml qml
QT += network websockets serialport
QT += sql
QT += multimedia
QT += script scripttools
QT += webenginewidgets webchannel
QT += concurrent
QT += gui-private

TEMPLATE = lib
Debug: TARGET=qtCoreD
Release: TARGET=qtCore
CONFIG += staticlib

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target

LIBS += -lwsock32
DESTDIR = $$PWD/libs
DLLDESTDIR = $$PRJDIR ../bin

INCLUDEPATH += $$PWD/extends/ffmpeg/include

CONFIG(release,debug|release){
    LIBS += $$PWD/extends/irrlicht/libs/Irrlicht.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avcodec.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avdevice.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avfilter.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avformat.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avutil.lib
    #LIBS += $$PWD/extends/ffmpeg/lib/postproc.lib
    #LIBS += $$PWD/extends/ffmpeg/lib/swresample.lib
    LIBS += $$PWD/extends/ffmpeg/lib/swscale.lib
}else{
    LIBS += $$PWD/extends/irrlicht/libs/Irrlicht.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avcodec.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avdevice.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avfilter.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avformat.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avutil.lib
    #LIBS += $$PWD/extends/ffmpeg/lib/postproc.lib
    #LIBS += $$PWD/extends/ffmpeg/lib/swresample.lib
    LIBS += $$PWD/extends/ffmpeg/lib/swscale.lib
}

RESOURCES += \
    libqtcore.qrc

HEADERS += \
    extends/debuger/ldb.h \
    extends/debuger/ldb_core.h \
    extends/debuger/ldb_file.h \
    extends/debuger/ldb_util.h \
    extends/lua5.4/fpconv.h \
    extends/lua5.4/lapi.h \
    extends/lua5.4/lauxlib.h \
    extends/lua5.4/lcode.h \
    extends/lua5.4/lctype.h \
    extends/lua5.4/ldebug.h \
    extends/lua5.4/ldo.h \
    extends/lua5.4/lfunc.h \
    extends/lua5.4/lgc.h \
    extends/lua5.4/ljumptab.h \
    extends/lua5.4/llex.h \
    extends/lua5.4/llimits.h \
    extends/lua5.4/lmem.h \
    extends/lua5.4/lobject.h \
    extends/lua5.4/lopcodes.h \
    extends/lua5.4/lopnames.h \
    extends/lua5.4/lparser.h \
    extends/lua5.4/lprefix.h \
    extends/lua5.4/lstate.h \
    extends/lua5.4/lstring.h \
    extends/lua5.4/ltable.h \
    extends/lua5.4/ltm.h \
    extends/lua5.4/lua.h \
    extends/lua5.4/lua.hpp \
    extends/lua5.4/luaconf.h \
    extends/lua5.4/lualib.h \
    extends/lua5.4/lundump.h \
    extends/lua5.4/lvm.h \
    extends/lua5.4/lzio.h \
    extends/lua5.4/strbuf.h \
    extends/nedmalloc/NedAllocatedObject.h \
    extends/nedmalloc/NedAllocatorImpl.h \
    extends/nedmalloc/malloc.c.h \
    extends/nedmalloc/nedmalloc.h \
    includes/3d/cirrlichtwidget.h \
    includes/QsLog/QsLog.h \
    includes/QsLog/QsLogDest.h \
    includes/QsLog/QsLogDestConsole.h \
    includes/QsLog/QsLogDestFile.h \
    includes/QsLog/QsLogDestFunctor.h \
    includes/QsLog/QsLogDisableForThisFile.h \
    includes/QsLog/QsLogLevel.h \
    includes/breakpad/auto_critical_section.h \
    includes/breakpad/breakpad_types.h \
    includes/breakpad/client_info.h \
    includes/breakpad/crash_generation_client.h \
    includes/breakpad/crash_generation_server.h \
    includes/breakpad/crash_report_sender.h \
    includes/breakpad/exception_handler.h \
    includes/breakpad/guid_string.h \
    includes/breakpad/http_upload.h \
    includes/breakpad/ipc_protocol.h \
    includes/breakpad/minidump_cpu_amd64.h \
    includes/breakpad/minidump_cpu_arm.h \
    includes/breakpad/minidump_cpu_arm64.h \
    includes/breakpad/minidump_cpu_mips.h \
    includes/breakpad/minidump_cpu_ppc.h \
    includes/breakpad/minidump_cpu_ppc64.h \
    includes/breakpad/minidump_cpu_sparc.h \
    includes/breakpad/minidump_cpu_x86.h \
    includes/breakpad/minidump_exception_fuchsia.h \
    includes/breakpad/minidump_exception_linux.h \
    includes/breakpad/minidump_exception_mac.h \
    includes/breakpad/minidump_exception_ps3.h \
    includes/breakpad/minidump_exception_solaris.h \
    includes/breakpad/minidump_exception_win32.h \
    includes/breakpad/minidump_format.h \
    includes/breakpad/minidump_generator.h \
    includes/breakpad/scoped_ptr.h \
    includes/breakpad/string_utils-inl.h \
    includes/common/singleton.h \
    includes/common/common.h \
    includes/common/rc4.h \
    includes/common/fastlz.h \
    includes/common/ctranslate.h \
    includes/common/singleapplication.h \
    includes/database/mssqldataprovider.h \
    includes/database/ndbpool.h \
    includes/database/ndbpool_p.h \
    includes/database/recordset.h \
    includes/database/sqlitedataprovider.h \
    includes/scripts/cjsmanager.h \
    includes/scripts/cluamanager.h \
    includes/network/cserialportmanager.h \
    includes/network/ctcpsocketclient.h \
    includes/network/ctcpsocketserver.h \
    includes/network/cudpsocket.h \
    includes/network/httplib.h \
    includes/network/csvnversionclient.h \
    includes/network/csvnversionserver.h \
    includes/network/cwebsocketclient.h \
    includes/network/cwebsocketserver.h \
    includes/network/networkframemanager.h \
    includes/network/JQLibrary/jqdeclare.hpp \
    includes/network/JQLibrary/jqhttpserver.h \
    includes/network/JQLibrary/jqnet.h \
    includes/widgets/libframelesswindow.h \
    includes/widgets/libmaskwidget.h \
    includes/network/libhttp.h \
    includes/network/ikcp.h \
    includes/multimedia/cremotevoice.h \
    includes/multimedia/avilib.h \
    includes/multimedia/czipmanager.h \
    includes/widgets/libcwebuimanager.h \
    includes/widgets/libcmessagebox.h

SOURCES += \
    extends/debuger/ldb.cpp \
    extends/debuger/ldb_file.cpp \
    extends/debuger/ldb_util.cpp \
    extends/lua5.4/fpconv.c \
    extends/lua5.4/lapi.c \
    extends/lua5.4/lauxlib.c \
    extends/lua5.4/lbaselib.c \
    extends/lua5.4/lcode.c \
    extends/lua5.4/lcorolib.c \
    extends/lua5.4/lctype.c \
    extends/lua5.4/ldblib.c \
    extends/lua5.4/ldebug.c \
    extends/lua5.4/ldo.c \
    extends/lua5.4/ldump.c \
    extends/lua5.4/lfunc.c \
    extends/lua5.4/lgc.c \
    extends/lua5.4/linit.c \
    extends/lua5.4/liolib.c \
    extends/lua5.4/llex.c \
    extends/lua5.4/lmathlib.c \
    extends/lua5.4/lmem.c \
    extends/lua5.4/loadlib.c \
    extends/lua5.4/lobject.c \
    extends/lua5.4/lopcodes.c \
    extends/lua5.4/loslib.c \
    extends/lua5.4/lparser.c \
    extends/lua5.4/lstate.c \
    extends/lua5.4/lstring.c \
    extends/lua5.4/lstrlib.c \
    extends/lua5.4/ltable.c \
    extends/lua5.4/ltablib.c \
    extends/lua5.4/ltm.c \
    extends/lua5.4/lua.c \
    extends/lua5.4/lua_cjson.c \
    extends/lua5.4/luac.c \
    extends/lua5.4/lundump.c \
    extends/lua5.4/lutf8lib.c \
    extends/lua5.4/lvm.c \
    extends/lua5.4/lzio.c \
    extends/lua5.4/strbuf.c \
    extends/nedmalloc/NedAllocatedObject.cpp \
    extends/nedmalloc/NedAllocatorImpl.cpp \
    extends/nedmalloc/nedmalloc.c \
    src/3d/cirrlichtwidget.cpp \
    src/breakpad/client_info.cc \
    src/breakpad/crash_generation_client.cc \
    src/breakpad/crash_generation_server.cc \
    src/breakpad/crash_report_sender.cc \
    src/breakpad/exception_handler.cc \
    src/breakpad/guid_string.cc \
    src/breakpad/http_upload.cc \
    src/breakpad/minidump_generator.cc \
    src/breakpad/string_utils.cc \
    src/common/common.cpp \
    src/common/rc4.c \
    src/common/ctranslate.cpp \
    src/common/fastlz.c \
    src/common/singleapplication.cpp \
    src/scripts/cjsmanager.cpp \
    src/scripts/cluamanager.cpp \
    src/network/cserialportmanager.cpp \
    src/network/ctcpsocketclient.cpp \
    src/network/ctcpsocketserver.cpp \
    src/network/cudpsocket.cpp \
    src/network/csvnversionclient.cpp \
    src/network/csvnversionserver.cpp \
    src/widgets/libcmessagebox.cpp \
    src/network/libhttp.cpp \
    src/QsLog/QsLog.cpp \
    src/QsLog/QsLogDest.cpp \
    src/QsLog/QsLogDestConsole.cpp \
    src/QsLog/QsLogDestFile.cpp \
    src/QsLog/QsLogDestFunctor.cpp \
    src/network/cwebsocketclient.cpp \
    src/network/cwebsocketserver.cpp \
    src/widgets/libframelesswindow.cpp \
    src/widgets/libmaskwidget.cpp \
    src/widgets/webui/libcwebuimanager.cpp \
    src/database/mssqldataprovider.cpp \
    src/database/ndbpool.cpp \
    src/database/ndbpool_p.cpp \
    src/database/sqlitedataprovider.cpp \
    src/network/networkframemanager.cpp \
    src/network/ikcp.cpp \
    src/database/recordset.cpp \
    src/multimedia/cremotevoice.cpp \
    src/multimedia/avilib.c \
    src/multimedia/czipmanager.cpp \
    src/network/JQLibrary/jqhttpserver.cpp \
    src/network/JQLibrary/jqnet.cpp

FORMS += \
    src/widgets/libcmessagebox.ui
