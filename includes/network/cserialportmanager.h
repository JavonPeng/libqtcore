#ifndef _C_SERIALPORT_MANAGER_H_INCLUDE_
#define _C_SERIALPORT_MANAGER_H_INCLUDE_

#include <QtCore/QtGlobal>
#include <QObject>

#include <QtSerialPort/QSerialPort>

#include "../common/singleton.h"
#include "../common/common.h"
#include "networkframemanager.h"

/**
 * @brief The CSerialPort class 串口操作类
 */
class CSerialPort : public QSerialPort
{
    Q_OBJECT

public:
    explicit CSerialPort(NetworkFrameManager *pNetworkFrameManager=NULL,QObject *parent = 0);
    ~CSerialPort();

    /// 查找系统中Arduino专用串口，并打开
    void openArduinoSerialPort();
    /// 打开串口
    bool openSerialPort(QString PortName,qint32 BaudRate,QSerialPort::DataBits DataBits,QSerialPort::Parity Parity,QSerialPort::StopBits StopBits,QSerialPort::FlowControl FlowControl);
    /// 关闭串口
    void closeSerialPort();
    /// 发送json数据
    void sendJson(QJsonObject mes);
    /// 发送二进制数据
    bool SendData(QByteArray &data);

private slots:
    qint64 writeData(const QByteArray &data);
    /// 读取接收到的数据
    void readData();

    void handleError(QSerialPort::SerialPortError error);

private:
    /// 解析信息包
    bool parsePacket(void);

private:
    QByteArray m_dataPacket;                    /**< 用于存放接收的数据 */
    bool m_recvDataState;                       /**< 用于接收数据处理 */
    tagPacketHearder m_PacketHearder;           /**< 用于存放每次接收的包头数据 */
    NetworkFrameManager *m_NetworkFrameManager;                         /**< 网络消息处理框架 */
};

/**
 * @brief The CSerialPortManager class 串口管理类
 */
class CSerialPortManager : public Singleton<CSerialPortManager>
{
public:
    CSerialPortManager();
    ~CSerialPortManager();

    /// 添加一个串口
    bool addSerialPort(QString name,CSerialPort* pSerialPort);
    /// 得到一个串口
    CSerialPort* getSerialPort(QString name);
    /// 清除所有的串口
    void clearAllSerialPorts(void);
    /// 清除指定的串口
    bool deleteSerialPort(QString name);

private:
    QHash<QString,CSerialPort*> m_SerialPortList;
};

#endif
