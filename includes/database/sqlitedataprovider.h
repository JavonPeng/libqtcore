#ifndef SQLITEDATAPROVIDER_H
#define SQLITEDATAPROVIDER_H

#include "recordset.h"
#include "../common/singleton.h"

#include <QObject>
#include <QString>

class SqliteDataProvider : public QObject, public Singleton<SqliteDataProvider>
{
    Q_OBJECT

public:
    /// 构造函数
    Q_INVOKABLE SqliteDataProvider(QObject *parent = nullptr);
    /// 析构函数
    ~SqliteDataProvider(void);

    /// 建立与数据库的连接
    Q_INVOKABLE bool connect(QString dbPath);
    /// 得到数据库文件路径
    Q_INVOKABLE inline QString getdbPath(void) { return m_dbPath; }
    /// 执行SQL语句
    Q_INVOKABLE const RecordSetList execSql(const QString& sql);
    /// 执行sql的insert语句
    Q_INVOKABLE const RecordSetList execInsertSql(const QString& sql);
    /// 关闭与数据库的连接
    Q_INVOKABLE void disconnect(void);

private:
    QString m_dbPath;              /**< 数据库路径 */
};

#endif // SQLITEDATAPROVIDER_H
