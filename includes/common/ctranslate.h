#ifndef CTRANSLATE_H
#define CTRANSLATE_H

#include "common.h"
#include "singleton.h"

#include <QObject>
#include <QVector>
#include <QList>
#include <QString>
#include <QHash>

/**
 * 国际化,采用单例模式，在字库文件加载成功和语言改变时会触发消息，在这两个消息中
 * 改变相应的控件，用法如下：
 *
 *  connect(m_translater,SIGNAL(loadEntrysFinished()),this,SLOT(process_loadEntrysFinished()));
 *  connect(m_translater,SIGNAL(switchLanguageFinished()),this,SLOT(process_switchLanguageFinished()));
 *
 *  void MainWindow::process_loadEntrysFinished(void)
 *  {
 *     ui->pushButton->setText(m_translater->getEntryTranslate("team"));
 *     ui->label->setText(m_translater->getEntryTranslate("controller"));
 *  }
 *
 * xml定义：
 *
 *   <?xml version="1.0" encoding="UTF-8"?>
 *   <internationalization>
 *       <languages>
 *           <language name="英语" />
 *           <language name="中文" />
 *       </languages>
 *       <entrys>
 *           <entry name="team">
 *               <translate value="team" />
 *               <translate value="班组" />
 *           </entry>
 *           <entry name="controller">
 *               <translate value="Abnormal access controller" />
 *               <translate value="门禁控制器异常" />
 *           </entry>
 *       </entrys>
 *   </internationalization>
 */
class CTranslate : public QObject, public Singleton<CTranslate>
{
    Q_OBJECT

public:
    CTranslate(QObject *parent = nullptr,int curLanguage=0);
    ~CTranslate();

    /// 导入翻译词汇配置文件
    bool loadEntrys(QString filepath);
    /// 得到当前使用的语言类型数量
    inline int getLanguageTypeCount(void) { return m_languagetypes.size(); }
    /// 得到当前使用的语言类型
    inline QStringList getLanguageTypes(void) { return QStringList(m_languagetypes); }
    /// 得到当前使用的语言索引
    inline int getCurrentUsedLanguage(void) { return m_currentSelectedLanguage; }
    /// 切换语言
    void SwitchLanguage(int languageIndex=-1);
    /// 得到指定名称的翻译
    QString getEntryTranslate(QString entryName);

signals:
    /// 词汇配置文件加载完成后调用
    void loadEntrysFinished(void);
    /// 用户切换不同的语言后调用
    void switchLanguageFinished(void);

private:
    QList<QString> m_languagetypes;                  /**< 要翻译的语言类型 */
    QHash<QString,QVector<QString>> m_entrys;          /**< 所有要翻译的词汇 */
    int m_currentSelectedLanguage;                     /**< 当前选中的语言 */
};

#define sCTranslate CTranslate::getSingleton()

#endif // CTRANSLATE_H
