﻿#ifndef LIB_MASKWIDGET_H
#define LIB_MASKWIDGET_H

#include <QWidget>

class MaskWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MaskWidget(QWidget *parent = 0);
    ~MaskWidget();

    /// 设置主窗口
    void setMainWidget(QWidget *widget);
    /// 设置调色板颜色
    void setPaletteColor(const QColor &color,float opacity);
    /// 设置顶层窗口
    void setTopWidget(QWidget *widget);
    /// 设置遮挡颜色
    void setMaskColor(QColor color);
    /// 设置是否圆角阴影
    inline void setShadeRounded(bool enabled) { _shadeRoundEnabled = enabled; m_shadeRoundEnabled = enabled; }

private:
    void init();
    void showEvent(QShowEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);

protected:
    // 重写实现窗口边框阴影效果
    virtual void paintEvent(QPaintEvent *event);
    virtual void changeEvent(QEvent *event);

private:
    QWidget *m_topWidget;
    QWidget *m_mainWidget;
    QColor m_color;
    bool   _shadeRoundEnabled,m_shadeRoundEnabled;
};

#endif // MASKWIDGET_H
