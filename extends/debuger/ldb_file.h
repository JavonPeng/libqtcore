﻿#ifndef __LDB_FILE_H__
#define __LDB_FILE_H__

#include "ldb_core.h"

ldb_file_t* ldb_file_load(ldb_t *ldb, const char *name);
void        ldb_file_free(ldb_file_t *file);

#endif 
