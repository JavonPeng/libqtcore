﻿#ifndef _LDB_H_INCLUDE_
#define _LDB_H_INCLUDE_

#include "ldb_core.h"

ldb_t*  ldb_new(lua_State *state, ldb_reload_pt reload);
void    ldb_free(ldb_t *ldb);

void    ldb_break(lua_State *state);
void    ldb_reset(lua_State *state);

void    ldb_startsocket(lua_State *state, const char *ipaddr, int port);
void    ldb_closesocket(void);
int		ldb_write(lua_State *L,const char *message);
int     ldb_read(lua_State *L,char *buf);

#endif
