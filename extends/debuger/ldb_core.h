﻿#ifndef __LDB_CORE_H__
#define __LDB_CORE_H__

#include <stdlib.h>
#include <string.h>

extern "C"
{
    #include "../lua5.4/lua.h"
    #include "../lua5.4/lauxlib.h"
    #include "../lua5.4/lualib.h"
}

#define MAX_FILE_BUCKET 20
#define MAX_BREAKPOINT  60

typedef void(*ldb_reload_pt)(lua_State *state, const char *file);

typedef struct ldb_breakpoint_t {
	unsigned int  available : 1;
	char         *file;
	char         *func;
	const char   *type;
	int           line;
	unsigned int  active : 1;
	int           index;
	int           hit;
} ldb_breakpoint_t;

typedef struct ldb_file_t {
	char       *name;
	char      **lines;
	int         alloc;
	int         line;
	ldb_file_t *next;
} ldb_file_t;

typedef struct ldb_t {
	lua_State        *state;

	ldb_reload_pt     reload;

	int               call_depth;
	int               step : 1;
	int               first : 1;

	ldb_file_t       *files[MAX_FILE_BUCKET];

	int               bknum;
	ldb_breakpoint_t  bkpoints[MAX_BREAKPOINT];
} ldb_t;

#endif  /* __LDB_CORE_H__ */
