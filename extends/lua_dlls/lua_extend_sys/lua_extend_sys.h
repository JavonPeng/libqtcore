﻿#ifndef LUA_EXTEND_SYS_H
#define LUA_EXTEND_SYS_H

#include "lua_extend_sys_global.h"
#include "../../../includes/scripts/cluamanager.h"

extern "C"
{
    #include "../../lua5.4/lua.h"
    #include "../../lua5.4/lauxlib.h"
    #include "../../lua5.4/lualib.h"
}

extern "C"
{
    LUA_EXTEND_SYS_EXPORT void reg_xxx2Luas(CLuaManager *g_lua_manager,lua_State *g_lua_state);
}

#endif // LUA_EXTEND_SYS_H
