﻿#include "lua_extend_sys.h"
#include "../../../includes/QsLog/QsLog.h"
#include "../../../includes/common/common.h"
#include "../../debuger/ldb.h"

CLuaManager *m_lua_manager = NULL;

static int c_break(lua_State *state) {
    ldb_break(state);
    return 0;
}

int setlog_tolua(lua_State* L)
{
    std::string type = luaL_checkstring(L, 1);
    std::string msg =  luaL_checkstring(L, 2);

    if (type == "error")
        QLOG_ERROR() << msg.c_str();
    else if (type == "info")
        QLOG_INFO() << msg.c_str();
    else if (type == "waring")
        QLOG_WARN() << msg.c_str();
    else if (type == "fatal")
        QLOG_FATAL() << msg.c_str();

    m_lua_manager->SendLog(msg.c_str(), __FILE__, __LINE__,type.c_str());

    return 0;
}

const struct luaL_Reg sysLib[] =
{
    {"log", setlog_tolua},
    {"lbreak", c_break},

    {NULL, NULL}       //数组中最后一对必须是{NULL, NULL}，用来表示结束
};

void reg_xxx2Luas(CLuaManager *g_lua_manager,lua_State *g_lua_state)
{
    lua_getglobal(g_lua_state, "sys");
    if (lua_isnil(g_lua_state, -1)) {
        lua_pop(g_lua_state, 1);
        lua_newtable(g_lua_state);
    }

    luaL_setfuncs(g_lua_state, sysLib, 0);
    lua_setglobal(g_lua_state, "sys");

    m_lua_manager = g_lua_manager;
}
