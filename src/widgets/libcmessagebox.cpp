﻿#include "../../includes/widgets/libcmessagebox.h"
#include "ui_libcmessagebox.h"

#include <QMessageBox>

namespace CMessageBox
{

libcmessagebox::libcmessagebox(QWidget *parent) :
    FramelessWindow(parent),
    ui(new Ui::libcmessagebox)
{
    ui->setupUi(this);

    setResize(false);
    setDoubleClickMaxsize(false);
}

libcmessagebox::~libcmessagebox()
{
    delete ui;
}

/**
 * @brief libcmessagebox::waring 警告
 * @param title 窗口标题
 * @param content 警告内容
 * @return 返回窗口状态
 */
int libcmessagebox::waring(QString title,QString content)
{
    ui->label->setText(title);
    ui->label_3->setText(content);

    ui->label_4->setPixmap(QPixmap(":/Resource/informationBlue_48.png"));

    ui->pushButton->hide();
    ui->pushButton_2->show();

    return this->exec();
}

/**
 * @brief libcmessagebox::information 信息
 * @param title 窗口标题
 * @param content 警告内容
 * @return 返回窗口状态
 */
int libcmessagebox::information(QString title,QString content)
{
    ui->label->setText(title);
    ui->label_3->setText(content);

    ui->label_4->setPixmap(QPixmap(":/Resource/successGreen_48.png"));

    ui->pushButton->hide();
    ui->pushButton_2->show();

    return this->exec();
}

/**
 * @brief libcmessagebox::question 问题
 * @param title 窗口标题
 * @param content 警告内容
 * @return 返回窗口状态
 */
int libcmessagebox::question(QString title,QString content)
{
    ui->label->setText(title);
    ui->label_3->setText(content);

    ui->label_4->setPixmap(QPixmap(":/Resource/questionBlue_48.png"));

    ui->pushButton->show();
    ui->pushButton_2->show();

    return this->exec();
}

void libcmessagebox::on_pushButton_close_clicked()
{
    this->done(-1);
}

void libcmessagebox::on_pushButton_2_clicked()
{
    this->done(QMessageBox::Yes);
}

void libcmessagebox::on_pushButton_clicked()
{
    this->done(QMessageBox::No);
}

/**
 * @brief Waring 警告
 * @param parent 当前对话框的父结点
 * @param title 当前对话框的标题
 * @param content 当前对话框的内容
 * @param isMask 是否使用遮挡效果
 * @return 返回点击后的状态
 */
int Waring(QWidget *parent,QString title,QString content,bool isMask)
{
    libcmessagebox m_libcmessagebox(parent);

    if(isMask && parent)
    {
        FramelessWindow *pFramelessWindow = static_cast<FramelessWindow*>(parent);
        if(pFramelessWindow) pFramelessWindow->setMaskWidget(&m_libcmessagebox);
    }

    return m_libcmessagebox.waring(title,content);
}

/**
 * @brief Information 信息
 * @param parent 当前对话框的父结点
 * @param title 当前对话框的标题
 * @param content 当前对话框的内容
 * @param isMask 是否使用遮挡效果
 * @return 返回点击后的状态
 */
int Information(QWidget *parent,QString title,QString content,bool isMask)
{
    libcmessagebox m_libcmessagebox(parent);

    if(isMask && parent)
    {
        FramelessWindow *pFramelessWindow = static_cast<FramelessWindow*>(parent);
        if(pFramelessWindow) pFramelessWindow->setMaskWidget(&m_libcmessagebox);
    }

    return m_libcmessagebox.information(title,content);
}

/**
 * @brief Question 问题
 * @param parent 当前对话框的父结点
 * @param title 当前对话框的标题
 * @param content 当前对话框的内容
 * @param isMask 是否使用遮挡效果
 * @return 返回点击后的状态
 */
int Question(QWidget *parent,QString title,QString content,bool isMask)
{
    libcmessagebox m_libcmessagebox(parent);

    if(isMask && parent)
    {
        FramelessWindow *pFramelessWindow = static_cast<FramelessWindow*>(parent);
        if(pFramelessWindow) pFramelessWindow->setMaskWidget(&m_libcmessagebox);
    }

    return m_libcmessagebox.question(title,content);
}

}
